
* # Presentation   

Word embedding is the collective name for a set of language modeling and feature learning techniques in natural language processing (NLP) where words or phrases from the vocabulary are mapped to vectors of real numbers. 

It's being use in almost all domain of NLP. Despite its many advantages, it does not really solve the problem of disambiguation. One of its weaknesses is that for given word 'poster' that can refer to either a "large printed picture used for decoration" or "a person who posts something online, as on a blog, social media website, or forum", it associates only one vector.

There is now a new technique called Sense Embedding that allows to associate with a word, a vector according to its meaning.  

In this work, we used this technique to compute for each word of "dbnary" a vector according to its definition in several languages.

	
* # Ressources 

## Model:	

There are many models proposed for sense embedding but in this article we used the one proposed by Loic VIAL, Benjamin LECOUTEUX , Didier SCHWAB.

In their model, they developed a new way of creating sense vectors for any dictionary, by using an existing word embeddings model, and summing the vectors of the terms inside a sense�s definition, weighted in function of their part of speech and their frequency.

Futher informations about this model can be found here : https://hal.archives-ouvertes.fr/hal-01599685/document .	

## Part Of Speech (POS) tagger :

We used Stanford one available here https://nlp.stanford.edu/software/tagger.shtml#Download and this one http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/.

Futher informations on abbreviations associated with positions can be found here : https://www.ling.upenn.edu/courses/Fall_2003/ling001/penn_treebank_pos.html .

##POS Weights : 

The model proposed associates a weight with each POS. For all languages, we used : 

Nouns : 1
Verbs : 0.75
Adjectives : 0.5
Adverbs : 0.25
Others : 0.25

## Pre-trained word's vectors : 

The model also includes the use of pre-trained word vectors. 

Here, we used fastText, Facebook's word embeddings library available on : https://fasttext.cc/docs/en/crawl-vectors.html
		
* # Evaluations

To test the performance of this model, we used it on translation disambiguation task. 
	
For a word and a context given, we tried to work out the most probable sense that is can have and used some similarity methods to evaluate the accuracy. 

* # Generated vectors and used program
	
This repository hosts only the program that generates vectors. It does not contain tools to use it (POS tagger, pre-trained words, ... ).

Files containing generated vectors for different languages in text and CSV formats are also available on this repository.
	
* # Preparing the environment

Add the following code in .bashrc
	
```bash
	export JENAROOT=.../apache-jena-3.3.0
	export TDBROOT=$JENAROOT
	export PATH="$PATH:$JENAROOT/bin"
```

Download the latest dbnary extracts (currently we only use 6 languages)

```bash
for l in de	en es fr el pt sv
do
    wget http://kaiko.getalp.org/static/ontolex/latest/${l}_dbnary_ontolex.ttl.bz2
	bunzip2 ${l}_dbnary_ontolex.ttl.bz2
done
```

Setup the TDB containing the dataset

```bash
tdbloader --set tdb:unionDefaultGraph=true --loc db --graph http://kaiko.getalp.org/dbnary/fra fr_dbnary_ontolex.ttl
tdbloader --set tdb:unionDefaultGraph=true --loc db --graph http://kaiko.getalp.org/dbnary/eng en_dbnary_ontolex.ttl
tdbloader --set tdb:unionDefaultGraph=true --loc db --graph http://kaiko.getalp.org/dbnary/deu de_dbnary_ontolex.ttl
tdbloader --set tdb:unionDefaultGraph=true --loc db --graph http://kaiko.getalp.org/dbnary/sve sv_dbnary_ontolex.ttl
tdbloader --set tdb:unionDefaultGraph=true --loc db --graph http://kaiko.getalp.org/dbnary/por pt_dbnary_ontolex.ttl
tdbloader --set tdb:unionDefaultGraph=true --loc db --graph http://kaiko.getalp.org/dbnary/ell el_dbnary_ontolex.ttl
tdbloader --set tdb:unionDefaultGraph=true --loc db --graph http://kaiko.getalp.org/dbnary/spa sp_dbnary_ontolex.ttl
```