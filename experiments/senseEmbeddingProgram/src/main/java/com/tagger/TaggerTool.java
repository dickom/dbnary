package com.tagger ;

import edu.stanford.nlp.util.logging.Redwood;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.List;

import edu.stanford.nlp.ling.SentenceUtils;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;

public class TaggerTool  {

  /** A logger for this class */
  //private static Redwood.RedwoodChannels log = Redwood.channels(TaggerTool.class);
  MaxentTagger tagger ;

  public TaggerTool () {
    this.tagger = new MaxentTagger("/Users/dickoma/IdeaProjects/FirstSenseEmbeddingTest/src/com/tagger/models/wsj-0-18-left3words-nodistsim.tagger");
  }


  public String tagText (String text){
      return this.tagger.tagString(text);
  }


}
