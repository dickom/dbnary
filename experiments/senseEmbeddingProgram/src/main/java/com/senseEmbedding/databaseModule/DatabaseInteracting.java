package com.senseEmbedding.databaseModule;

import com.senseEmbedding.evaluation.Words;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Hashtable;


/**
 * @author Mamadou DICKO
 */
public class DatabaseInteracting  extends Thread  {

    String fileName ;
    String tableName ;

    public DatabaseInteracting(String fileName ,  String tableName) throws SQLException, ClassNotFoundException {

        this.fileName = fileName ;
        this.tableName = tableName ;

    }

    public void run (){
        try {
            this.loadSenseVectorInDataBase (this.fileName ,  this.tableName) ;
        } catch (SQLException e) {
            e.printStackTrace ();
        } catch (ClassNotFoundException e) {
            e.printStackTrace ();
        }
    }

    public static void  loadSenseVectorInDataBase (String fileName ,  String tableName) throws SQLException, ClassNotFoundException  {

        System.out.println ("Loading "+fileName+" in "+tableName+" table.");

        Database database = new Database () ;
        if(database.con != null ){
            PreparedStatement preparedStmt ;

            /*
             *  Loading englishSenseVectors in database
             */

            BufferedReader br = null;
            FileReader fr = null;
            try {
                fr = new FileReader(fileName);
                br = new BufferedReader(fr);

                String sCurrentLine;
                int compteur = 0 ;

                while ((sCurrentLine = br.readLine()) != null) {

                    String wordAndVector [] = sCurrentLine.split (":") ;

                    String query = " insert into "+tableName+" (`Words`, `Vectors`)"
                            + " values (?,?)";


                    preparedStmt = database.con.prepareStatement(query);

                    try{
                        preparedStmt.setString (1,  wordAndVector [0]);
                        preparedStmt.setString (2,  wordAndVector [1]);
                        preparedStmt.execute();
                        compteur++ ;
                    }
                    catch (ArrayIndexOutOfBoundsException e) {
                        System.err.println ("OutOfBoundsError "+e.getMessage ());
                    }

                    if((compteur%1000)==0){
                        System.out.println ("Realized : " + compteur);
                    }
                }

                System.out.println ("Data in "+fileName+" loaded in "+tableName+" - Size: "+compteur);


            } catch (IOException e) {

                System.out.println("Mess "+e.getMessage ());

            } finally {

                try {

                    if (br != null)
                        br.close();

                    if (fr != null)
                        fr.close();

                } catch (IOException ex) {

                    ex.printStackTrace();

                }

            }

        }
        else
        {
            System.out.println(" Error ") ;
        }

    }

    public static void main (String[] args) throws SQLException, ClassNotFoundException {
     //   loadSenseVectorInDataBase("/Users/dickoma/development/dbnary/experiments/senseEmbeddingProgram/outcomes/EnglishSenseVectors","EnglishSensesVectors") ;
      //  new DatabaseInteracting("/Users/dickoma/development/dbnary/experiments/senseEmbeddingProgram/outcomes/EnglishSenseVectors","EnglishSensesVectors").start();
      new DatabaseInteracting("/Users/dickoma/development/dbnary/experiments/senseEmbeddingProgram/outcomes/GreekSenseVectors","GreekSenseVectors").start();
    }
}
