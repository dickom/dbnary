package com.senseEmbedding.databaseModule;

import java.sql.*;
import java.util.Properties;

/**
 * @author Mamadou DICKO
 */
public class Database {

    Connection con ;
    ResultSet résultats ;
    String request = "";
    Statement statement ;
    ResultSet rs ;

    public Database() throws SQLException, ClassNotFoundException {

        this.con = this.getConnection () ;
    }

    public Connection getConnection() throws SQLException {
        Connection conn = null ;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/wordSenseVectors";
            String user = "root";
            String password = "";
            conn = DriverManager.getConnection(url, user, password);
        }
        catch(Exception e) {
            System.out.println (e);
        }
        return conn;
    }

    public void closeConnexion () throws SQLException {
        this.con.close ();
    }

}
