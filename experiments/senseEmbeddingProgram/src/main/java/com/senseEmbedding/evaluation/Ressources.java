package com.senseEmbedding.evaluation;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.io.IOException ;

/**
 * @author Mamadou DICKO
 */
public class Ressources {


    private Hashtable<String , Words> vectors ;

    int base ;
    public Hashtable<String , Words> getVectors(){
        return this.vectors ;
    }

    public Ressources(String fileAdress, int all ){
        this.vectors = new Hashtable<String , Words> () ;
        System.out.println ("Recovering trained data from : "+fileAdress);
        BufferedReader br = null;
        FileReader fr = null;
        int compteur = 0 ;
        base = 300 ;
        try {

            //br = new BufferedReader(new FileReader(FILENAME));
            fr = new FileReader(fileAdress);
            br = new BufferedReader(fr);

            String sCurrentLine;
            if(all == 1){
                while ((compteur<400000)&(sCurrentLine = br.readLine()) != null) {
                    this.addRessources(sCurrentLine) ;
                    compteur++ ;
                    if( (compteur%10000)==0){
                        System.out.println ("Realized : " + compteur);
                    }
                }
            }
            else
            {

                while ((compteur<400000)&(sCurrentLine = br.readLine()) != null) {
                    this.addRessources2(sCurrentLine) ;
                    compteur++ ;
                    if((compteur%10000)==0){
                        System.out.println ("Realized : " + compteur);
                    }
                }
            }


        } catch (IOException e) {

            System.out.println(e.getMessage ());

        } finally {

            try {

                if (br != null)
                    br.close();

                if (fr != null)
                    fr.close();

            } catch (IOException ex) {

                ex.printStackTrace();

            }

        }


    }

    public void afficher(){
        Enumeration names;
        names = vectors.keys();
        String keyWriter ;
        while(names.hasMoreElements()) {
            keyWriter = (String)names.nextElement();
            System.out.print ("Key "+keyWriter+" ") ;
            vectors.get(keyWriter).afficher();
        }
    }
    public boolean estChiffre(char c){
        return ((c<='9')&(c>='0')) ;
    }

    public void addRessources(String wordsAndCoord) {
        int i = 0 ;
        String word ="" ;
        String currentWord = "" ;
        while((i < wordsAndCoord.length ()) && wordsAndCoord.charAt(i) !=' ' ){
            word += wordsAndCoord.charAt(i) ;
            i++ ;
        }
        Double [] currentVectors = new Double[base] ;
        boolean problem = false ;
        int j = 0 ;
        while((i < wordsAndCoord.length ())&&(wordsAndCoord.charAt (i) != '\n')){

            String coordinate = "" ;

            while((i < wordsAndCoord.length ())&&(wordsAndCoord.charAt (i) == ' ')){
                i++ ;
            }

            while((i < wordsAndCoord.length ())&&(wordsAndCoord.charAt (i) != ' ')&&(wordsAndCoord.charAt (i) != '\n')){
                coordinate+=wordsAndCoord.charAt (i) ;
                i++ ;
            }

            if(!coordinate.equals ("")){
                try {
                    currentVectors[j] = Double.parseDouble (coordinate) ;
                    j++ ;
                }
                catch (NumberFormatException e) {
                    problem = true  ;
                }

            }

        }
        if(!problem){
            i = 0 ;
            while((i != word.length ())&&(!estChiffre (word.charAt(i)))){ // // recovering current word senseNumber
                i++ ;
            }
            String senseNumber ="" ;
            while((i < word.length ())&&estChiffre (word.charAt(i))){ // // recovering current word senseNumber
                senseNumber += word.charAt(i) ;
                i++ ;
            }

            while((i < word.length ())&&(word.charAt(i) == '_')){
                i++ ;
            }

            while(i < word.length ()){ // recovering current word  name without the sense number
                currentWord += word.charAt(i);
                i++ ;
            }

            //System.out.println("Current word "+currentWord+" sense number "+senseNumber) ;
            this.addWord(currentWord,senseNumber,currentVectors) ;
        }

    }
    public void addRessources2(String wordsAndCoord) {
        int i = 0 ;
        String word ="" ;
        String currentWord = "" ;
        while((i < wordsAndCoord.length ())&& wordsAndCoord.charAt(i) !=' ' ){
            word += wordsAndCoord.charAt(i) ;
            i++ ;
        }

        Double [] currentVectors = new Double[base] ;
        int j = 0 ;
        boolean problem = false ;
        while((i < wordsAndCoord.length ())&&(wordsAndCoord.charAt (i) != '\n')){

            String coordinate = "" ;

            while((i < wordsAndCoord.length ())&&(wordsAndCoord.charAt (i) == ' ')){
                i++ ;
            }

            while((i < wordsAndCoord.length ())&&(wordsAndCoord.charAt (i) != ' ')&&(wordsAndCoord.charAt (i) != '\n')){
                coordinate+=wordsAndCoord.charAt (i) ;
                i++ ;
            }

            if(!coordinate.equals ("")){
                try {
                    currentVectors[j] = Double.parseDouble (coordinate) ;
                    j++ ;
                }
                catch (NumberFormatException e) {
                    problem = true  ;
                }

            }

        }
        if(!problem){
            i = 0 ;
            while((i < word.length ())&&(!estChiffre (word.charAt(i)))){ // // recovering current word senseNumber
                i++ ;
            }
            String senseNumber ="" ;
            while((i < word.length ())&&estChiffre (word.charAt(i))){ // // recovering current word senseNumber
                senseNumber += word.charAt(i) ;
                i++ ;
            }

            while((i < word.length ())&&(word.charAt(i) == '_')){
                i++ ;
            }

            int starting = i ;

            i = word.length ()-1 ;

            while((i>0)&&(word.charAt(i) != '_')){
                i-- ;
            }
            while((i>0)&&(word.charAt(i) == '_')){
                i-- ;
            }
            while((i>0)&&(word.charAt(i) != '_')){
                i-- ;
            }
            while((i>0)&&(word.charAt(i) == '_')){
                i-- ;
            }

            while(starting <= i ){ // recovering current word  name without the sense number
                currentWord += word.charAt(starting);
                starting++ ;
            }

       //      System.out.println("Current word adding "+currentWord+" "+senseNumber+" "+currentVectors) ;
            this.addWord(currentWord,senseNumber,currentVectors) ;
        }

    }

    public void addWord(String currentWord, String senseNumber,  Double [] vector){
    //     System.out.println("Adding "+currentWord) ;
        if(vectors.get(currentWord) == null ){
            this.vectors.put(currentWord, new Words (currentWord,senseNumber,vector)) ;
        }
        else
        {
            this.vectors.get(currentWord).addNewSense(senseNumber,vector)  ;
        }
    }

    public Double [] getWordVector(String word , String senseNumber){

        if(this.vectors.get(word) != null){
            return this.vectors.get(word).getVector(senseNumber) ;
        }
        else
        {
            return new Double [0] ;
        }
    }

    public Hashtable<String, Double [] >  getWordVector(String word){
      //  System.out.println("Current word "+word+" null ? "+(this.vectors.get(word) == null)) ;
        if(this.vectors.get(word) != null){
            return this.vectors.get(word).getVectors() ;
        }
        else
        {
            return new Hashtable< String,  Double [] >  ()   ;
        }
    }

}
