package com.senseEmbedding.evaluation;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * @author Mamadou DICKO
 */
public class Words {

    private String word ;
    private Hashtable<String, Double [] > vector ;
    int base ;

    public Words(String word, String position ,  Double [] vector){
        this.word = word ;
        this.base = 300 ;
        this.vector = new Hashtable<String, Double [] > () ;
        this.addNewSense(position,vector) ;

    }

    public Hashtable<String, Double [] >  getVector(){
        return this.vector ;
    }



    public String getName(){
        return this.word ;
    }

    public void afficherTab(Double [] vector1 ){
        for(int i = 0 ; i < vector1.length ; i++){
            if(vector1[i] != null){
                System.out.print (vector1[i].doubleValue()+" ");
            }


        }
    }

    public void afficher(){
        Enumeration names;
        names = vector.keys();
        String keyWriter ;
        while(names.hasMoreElements()) {
            keyWriter = (String)names.nextElement();
            System.out.print  (keyWriter +" : ");
            afficherTab (vector.get(keyWriter)) ;
        }
        System.out.println() ;
    }
    public void addNewSense(String position , Double [] vector){

      // System.out.println("Position : "+position+" vector "+vector) ;

        if((vector != null )&&(position != "")&&(vector != null)&&(position != null)){
            this.vector.put(position , vector ) ;
        }

    }

    public Double [] getVector(String senseNumber){
        if(this.vector.get(senseNumber) != null) {
            return this.vector.get(senseNumber) ;
        }
        else
        {
            return new Double [0] ;
        }

    }
    public Hashtable<String, Double [] > getVectors(){
        return this.vector ;
    }
}
