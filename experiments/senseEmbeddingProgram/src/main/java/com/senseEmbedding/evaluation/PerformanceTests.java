package com.senseEmbedding.evaluation;


import com.senseEmbedding.QuerySparqlJava;

import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;

public class PerformanceTests {

    private Ressources sourceVectors;
    private Ressources targetVectors;
    private String fileName ;
    File outputFile ;
    FileWriter out ;
    Hashtable<String , String >  outcomes ;
    int noSenseVector ;
    int noTranslated ;
    double correctAnswer ;
    double numberGenerated ;
    int fromPretarined ;
    int total  ;
    private Hashtable<String,  Double [] > pretrainedVectors ;
    public PerformanceTests(String sourceLanguage , String targetLanguage ) throws IOException {

        this.sourceVectors = new Ressources (sourceLanguage,1) ;
        this.targetVectors = new Ressources (targetLanguage,0) ;


        fileName= "statsFile.txt" ;
        outputFile = new File(this.fileName);
        out = new FileWriter(outputFile);
        outcomes =  new  Hashtable<String , String >() ;
        noSenseVector = 0 ;
        noTranslated = 0 ;
        correctAnswer = 0 ;
        numberGenerated = 0 ;
        this.fromPretarined = 0 ;
        this.total = 0 ;
        // this.targetVectors.afficher ();
        this.pretrainedVectors = new Hashtable<String,Double [] > ();
        this.loadEmbeddedWords("/Users/dickoma/Downloads/cc.fr.300.vec") ;

    }

    public void loadEmbeddedWords(String pretainedWordsVectorsPath) throws IOException {
            System.out.println("RECOVERING PRE-TRAINED WORDS VECTORS ...");
            File inputFile = new File(pretainedWordsVectorsPath) ;

            FileReader in = new FileReader(inputFile);
            int c;
            String mot = "" ;
            String mot2 ="" ;
            int compteur = 0 ;
            while ((c = in.read()) != '\n') {}
            int compt = 0 ;
            while ( (compteur<100000) & (c = in.read()) != -1) {
                compt++ ;
                while (c != ' ') {
                    mot += (char)c ;
                    c = in.read() ;
                }
                while (c == ' ') {
                    c = in.read() ;
                }
                int count = 0 ;

                Double [] coordinatesArray = new Double [ 300] ;

                while(count != 300){

                    while ((c != ' ')&&(c!='\n')) {
                        mot2 += (char)c ;
                        c = in.read() ;
                    }
                    coordinatesArray[count] = Double.parseDouble(mot2) ;
                    while ((c == ' ')) {
                        c = in.read() ;
                    }
                    count++ ;
                    mot2 = "" ;

                }
                while (c != '\n') {
                    c = in.read() ;
                }
//            System.out.println("Mot "+mot);
             //   this.pretrainedVectors.put(mot,coordinatesArray) ;
                compteur++ ;
                if((compteur%10000)==0){
                    System.out.println("Analyzed : " + compteur) ;
                }
                mot = "" ;
                mot2 = "" ;
            }
            System.out.println("Analyzed : " + compteur) ;
            System.out.println("END OF THE RECOVERY");
    }

    public void deconnect() throws IOException {
        this.out.close();
    }

    public void allWordsSimilarity() throws Exception {
        System.out.println ("Similarity working out... ");
        Hashtable<String , Words> vectors = this.sourceVectors.getVectors () ;

        Enumeration names;
        names = vectors.keys();
        String sourceWord ;
        while(names.hasMoreElements()) {
            sourceWord = (String)names.nextElement();
            Words currentWord = vectors.get(sourceWord);
            Enumeration curentWordSenses;
            curentWordSenses = currentWord.getVector().keys();
            String sourceSense ;
            while(curentWordSenses.hasMoreElements()) {
                sourceSense = (String)curentWordSenses.nextElement();
                this.similarity(sourceWord,sourceSense);
            }
        }
    }

    //  Word1 from first ressource , word2 from the second           System.out.print("Similarity : "+result+"\t") ;
    public boolean estChiffre(char c){
        return ((c<='9')&(c>='0')) ;
    }

    public String getResponse(String word){
        String sparqlService = "http://kaiko.getalp.org/sparql" ;
        String apikey = "dbnary";
        String query = "PREFIX dbnary: <http://kaiko.getalp.org/dbnary#>\n"
                +"SELECT DISTINCT ?t ?f WHERE {  \n"+
                "?t dbnary:isTranslationOf dbnary-eng:"+word+";  \n"+
                "dbnary:targetLanguage lexvo:fra ;  \n"+
                "dbnary:writtenForm ?f .  \n"+
                "    OPTIONAL {?t dbnary:gloss ?o}  \n"+
                " }" ;

        QuerySparqlJava execQuery = new QuerySparqlJava (sparqlService, apikey,query);

        StringWriter response = new StringWriter () ;

        try {
            execQuery.executeQuery("text/tab-separated-values", response);

        }
        catch (Exception e) {
            System.out.println (e.getMessage ());
        }
        return response.toString () ;
    }

    public Hashtable<String,  Hashtable<String, Double [] >  > getPossibility (String response, String currentWord) {

        Hashtable<String, Hashtable<String, Double[]>> vectors2 = new Hashtable<String, Hashtable<String, Double[]>> ();

        int i = 0;

        if (i != response.length ()) {

            String correctSense = "";
            while (i < response.length ()) {

                int j = i;

                while ((j < response.length ()) && !estChiffre (response.charAt (j))) {
                    j++;
                }

                String senseNumber = "";
                while ((j < response.length ()) && estChiffre (response.charAt (j))) {
                    senseNumber += response.charAt (j);
                    j++;
                }

                while ((i < response.length ()) && response.charAt (i) != '"') {
                    i++;
                }
                i++;
                while ((i < response.length ()) && response.charAt (i) != '"') {
                    i++;
                }
                i++;
                while ((i < response.length ()) && response.charAt (i) != '"') {
                    i++;
                }
                i++;
                while ((i < response.length ()) && (response.charAt (i) != '"')) {
                    correctSense += response.charAt (i);
                    i++;
                }
                while ((i < response.length ()) && response.charAt (i) != '\n') {
                    i++;
                }
                i++;

                correctSense = correctSense.replaceAll (" ","_");

                String value = senseNumber + "_" + currentWord + " : c -> " + correctSense + " | o -> ";
                System.out.println ("adding "+senseNumber+"_"+currentWord+" - "+value+" correct sense "+correctSense);
                this.outcomes.put(senseNumber+"_"+currentWord,value);
                if(targetVectors.getWordVector (correctSense) == null){
                    this.noSenseVector++ ;
                }
                vectors2.put (correctSense,targetVectors.getWordVector (correctSense));
                correctSense = "";
            }
        }
        return vectors2 ;
    }


    public  Hashtable<String, Double >  outcomesForOneWord( String currentMatchingWord , Hashtable<String, Double [] >  vectors2,String currentWord, String word1senseNumber,  Hashtable<String, Double > draft ) {

        String outcomeWord = "" ;

        Double [] vector1 = sourceVectors.getWordVector(currentWord , ""+word1senseNumber) ; // main word vector

        Hashtable<String, Double >  similarityWorkedOut =  new Hashtable<String, Double > () ;



        if(vectors2 != null ) {
            Enumeration matchingWordDifferentVectors = vectors2.keys (); //Like 1 , 2 ... sense number
            String senseNumber;
            double similarity = 0;
            while (matchingWordDifferentVectors.hasMoreElements ()) {
                senseNumber = (String) matchingWordDifferentVectors.nextElement ();
                similarity = cosinusSimilarity (vector1,vectors2.get (senseNumber));
                if(similarity != 0){
                    similarityWorkedOut.put (currentMatchingWord + "_" + senseNumber,similarity);
                }
            }
            if ((similarity == 0) && (this.pretrainedVectors.get (currentMatchingWord) != null)) {
                similarity = cosinusSimilarity (vector1,this.pretrainedVectors.get (currentMatchingWord));
                if(similarity != 0){
                    similarityWorkedOut.put (currentMatchingWord,similarity);
                }
                this.fromPretarined++;
            }
        }
        else
        {
            if(this.pretrainedVectors.get (currentWord) != null){

                double similarity = cosinusSimilarity (vector1,this.pretrainedVectors.get (currentWord));
                similarityWorkedOut.put (currentMatchingWord,similarity);
                this.fromPretarined++;

            }
        }
        return this.getMaxValue(similarityWorkedOut,draft) ;
    }

    public void similarity (String sourceWord, String senseNumber) throws Exception {

        this.total++ ;
     //   System.out.println("Word is "+word1) ;

        String response = this.getResponse (sourceWord) ; // return possible translation for the soureWord
        if(!response.equals ("")){
            this.noTranslated++ ;
        }
   //     System.out.println("rep "+response) ;

        Hashtable<String,  Hashtable<String, Double [] >  > vectors2 = this.getPossibility (response,sourceWord) ; // return possible sense vectors for translations

        Enumeration namesForWords  = vectors2.keys();

        String word ;

        if((vectors2.size () != 0) ){
            Hashtable<String, Double > draft = new Hashtable<String, Double > ();
            while(namesForWords.hasMoreElements()) {
                word = (String)namesForWords.nextElement(); // Word like "Bonjour"
          //      System.out.println("word issss "+word) ;
       //         this.afficherResults ( vectors2.get (word));
                Hashtable<String, Double [] > currentWordArray = vectors2.get(word) ; //recovering word's vectors

                if((currentWordArray != null ) && (currentWordArray.size () != 0 )){
                    draft = this.outcomesForOneWord(word,currentWordArray,sourceWord,senseNumber,draft) ;
                }
            }
            sourceWord = senseNumber+"_"+sourceWord ;
            if(!draft.isEmpty()) {
                if(this.outcomes.get(sourceWord) != null ){

                    if(!getMaxValueString (draft).equals("")){
                        this.numberGenerated++ ;
                        if(simplified(getMaxValueString (draft)).equals(expectedAnswer(this.outcomes.get(sourceWord)))){
                            this.correctAnswer++ ;
                        }
                        this.outcomes.put(sourceWord, this.outcomes.get(sourceWord)+getMaxValueString (draft)) ;
                    }
                    else{
                        this.outcomes.remove (sourceWord) ;
                    }

                }
                else{
                    this.outcomes.remove (sourceWord) ;
                }

            }
            else
            {
                this.outcomes.remove (sourceWord) ;
            }

        }
        else
        {
            sourceWord = senseNumber+"_"+sourceWord ;
            this.outcomes.remove (sourceWord) ;
          //  System.out.print (" 0 ");
        }


    }

    public String simplified(String deleteUnderscore){
        int i = 0 ;
        String response = "" ;
        while((i != deleteUnderscore.length ())&&(deleteUnderscore.charAt (i) != '_')){
            response+=deleteUnderscore.charAt (i) ;
            i++ ;
        }
        return response ;
    }

    public String expectedAnswer(String answer){
        String response = "" ;
        int i = 0 ;

        while((i < answer.length ()) &&(answer.charAt (i) != '>')){
            i++ ;
        }
        i = i+2 ;
        while((i < answer.length ()) &&(answer.charAt (i) != ' ')){
            response+= answer.charAt (i) ;
            i++ ;
        }
        return response ;
    }

    public void sauvegarder() throws IOException {
        Enumeration names3;
        names3 = this.outcomes.keys();
        String keyWriter = ""  ;
        out.write("Realized "+this.numberGenerated+" / "+this.total );
        out.write("\n");
        out.write("No sense vector "+this.noSenseVector);
        out.write("\n");
        out.write("No translation "+this.noTranslated);
        out.write("\n");
        out.write("Correct "+this.correctAnswer);
        out.write("\n");
        out.write("From Pretrained "+this.fromPretarined);
        out.write("\n");
        if(numberGenerated!=0){
            out.write("Accuracy "+(correctAnswer/numberGenerated)*100+" %");
            out.write("\n");
        }

        while(names3.hasMoreElements()) {

            keyWriter = (String)names3.nextElement();
            out.write(this.outcomes.get(keyWriter));
            out.write("\n");


        }

        this.deconnect() ;
        System.out.println("Saved in "+outputFile.getAbsolutePath()) ;
    }

    public  Hashtable<String, Double > getMaxValue( Hashtable<String, Double > tab, Hashtable<String, Double > draft ) {

        Enumeration names;
        names = tab.keys();
        String keyWriter ;
        String text = "" ;
        double value = 0 ;
        while(names.hasMoreElements()) {
            keyWriter = (String)names.nextElement();
            if(value < tab.get (keyWriter).doubleValue ()){
                value = tab.get (keyWriter).doubleValue () ;
                text = keyWriter ;
            }
        }
        draft.put(text,value) ;
        return (draft) ;
    }
    public  String getMaxValueString( Hashtable<String, Double > tab ) {

        Enumeration names;
        names = tab.keys();
        String keyWriter ;
        String text = "" ;
        double value = 0 ;
        while(names.hasMoreElements()) {
            keyWriter = (String)names.nextElement();
            if(value < tab.get (keyWriter).doubleValue ()){
                value = tab.get (keyWriter).doubleValue () ;
                text = keyWriter ;
            }
        }

        return (text) ;
    }

    public void afficherDoubleTab (Double [] tab){
        for (int i = 0 ; i < tab.length; i++) System.out.print ( tab[i].doubleValue ()+" ");
    }
    public void afficherResults(Hashtable<String, Double [] > tab){
        System.out.println ("Affichage 2");
        Enumeration names;
        names = tab.keys();
        String keyWriter ;
        while(names.hasMoreElements()) {
            keyWriter = (String)names.nextElement();
            System.out.print (keyWriter +" : ");
            this.afficherDoubleTab(tab.get(keyWriter)) ;
        }
        System.out.println ("Fin Affichage 2");
    }
    public void afficherResults(Hashtable<String, Double >  tab , int a ){
        Enumeration names;
        names = tab.keys();
        String keyWriter ;
        while(names.hasMoreElements()) {
            keyWriter = (String)names.nextElement();
            System.out.println (keyWriter +" : "+tab.get(keyWriter).doubleValue ());
        }
    }
    public void afficherResults( Hashtable<String,  Hashtable<String, Double [] >  >  tab , int a , int b  ){
        System.out.println ("Affichage ");
        Enumeration names;
        names = tab.keys();
        String keyWriter ;
        while(names.hasMoreElements()) {
            keyWriter = (String)names.nextElement();
            this.afficherResults(tab.get(keyWriter)) ;
        }
        System.out.println ("Fin affichage ");
    }

    public double cosinusSimilarity(Double firstVector [] , Double secondVector []){

        if((firstVector.length != 0) && (firstVector.length != 0 )){
            double fxy = 0 ;
            double qx = 0 ;
            double qy = 0 ;

            int i = 0 ;
            if(firstVector.length == secondVector.length){
                while(i != firstVector.length){
                    fxy += (firstVector[i].doubleValue ()*secondVector[i].doubleValue ()) ;
                    qx += (firstVector[i].doubleValue ()*firstVector[i].doubleValue ()) ;
                    qy += (secondVector[i].doubleValue ()*secondVector[i].doubleValue ()) ;
                    i++ ;
                }
            }
            if((qx != 0 )&&(qy !=0)){
                return  Math.sqrt(fxy/Math.sqrt(qx*qy)) ;
            }
            else
            {
                return 0 ;
            }

        }
        else
        {
            return 0 ;
        }

    }

    public double  distanceSimilarity(Double firstVector [] , Double secondVector []){

        double distance = 0 ;
        int i = 0 ;
        if(firstVector.length == secondVector.length){
            while(i != firstVector.length){

                double product = (firstVector[i].doubleValue ()-secondVector[i].doubleValue ()) ;

                product*= product ;

                distance += product ;

                i++ ;
            }
        }

        return Math.sqrt(distance);
    }

}
