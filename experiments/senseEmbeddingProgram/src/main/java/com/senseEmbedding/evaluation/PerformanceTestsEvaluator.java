package com.senseEmbedding.evaluation;

/**
 * @author Mamadou DICKO
 */
public class PerformanceTestsEvaluator {

    public static void main(String[] args) throws Exception {

        PerformanceTests perf = new PerformanceTests ("/Users/dickoma/development/dbnary/experiments/senseEmbeddingProgram/outcomes/EnglishSenseVectors","/Users/dickoma/development/dbnary/experiments/senseEmbeddingProgram/outcomes/FrenchSenseVectors") ;
        perf.allWordsSimilarity() ;
        perf.sauvegarder() ;

    }
}

