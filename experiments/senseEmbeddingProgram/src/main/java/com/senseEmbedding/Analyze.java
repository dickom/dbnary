package com.senseEmbedding;

/**
 * @author Mamadou DICKO
 */

import edu.stanford.nlp.tagger.maxent.MaxentTagger;

import java.util.Hashtable;
import java.util.Enumeration;
//import edu.stanford.nlp.tagger.maxent.MaxentTagger;
//import com.tagger.TaggerTool;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class Analyze {

    private int i ;
//    private TaggerTool tagger ;
    private ArrayList<String> mylist ;
    private Hashtable<String, Integer> hashtable ;
    private int compteur ;
    private MaxentTagger tagger2 ;
    private String doc ;

    public Analyze(String doc , String tagger_path) {
        this.tagger2= new MaxentTagger(tagger_path);
        this.mylist = new ArrayList<String>();
        this.hashtable = new Hashtable<String, Integer>();
        this.compteur = 0 ;
        this.doc = doc ;
    }

    public boolean notAvailable(char car ){
            return ((car=='"')||(car==' ')||(car=='_')||(car=='.')||(car==';')||(car==',')||(car=='(')||(car==')')||(car=='#')||(car=='\n')||(car==':') ||(car=='\n')||(car=='\'')||(car=='/') ||(car=='[') ||(car==']')||(car=='_')||(car=='\\')||(car=='<')||(car=='!')||(car=='-')||(car=='|')||(car=='`')||(car=='≡')||(car=='%')||(car=='$')||(car=='=')||(car=='’')||(car=='?')||(car=='$'));
    }


    public void occurrence_counter(String key){

        // Adding Key and Value pairs to Hashtable
        if( hashtable.get(key) != null ){
            hashtable.put(key, hashtable.get(key)+1);
        }
        else
        {
            hashtable.put(key, 1);
        }

    }

    public void analyzeQuery(){

        String answer = this.doc ;

        System.out.println("Analysing query ...");
        int i = 0;
        String motCourant="" ;
        String def ="" ;

        Hashtable<String, Boolean> did  ;

        while((i< answer.length())&&(answer.charAt(i) != '\n')){ //
            i++ ;
        }
        i++ ;
        while (i < answer.length()) {

            did = new Hashtable<String, Boolean>();

            while((i < answer.length())&&((answer.charAt(i) == ' ')||(answer.charAt(i) == '"'))){ // Starting with new definition
                i++ ;
            }
            while( i < answer.length()&&(answer.charAt(i) != '"')){
                motCourant+= answer.charAt(i) ;
                i++ ;
            }
            compteur++ ;
           // System.out.println("Mot courant "+motCourant);

            if((compteur%1000)==0){
                System.out.println("Number of definitions analyzed : "+compteur);
            }

            while((i< answer.length())&&((notAvailable(answer.charAt(i))))){ // Filtering11
                i++ ;
            }

            while( i < answer.length()&&(answer.charAt(i) != '"')){
                String words ="" ;

                while(i < answer.length()&&(!notAvailable(answer.charAt(i)))){
                    words+=answer.charAt(i) ;
                    i++ ;
                }

                if ((!(words.trim().equals("")))&&!((words.length() == 1)&&((words.charAt(0)!='a')&&(words.charAt(0)!='à')))){

                    if(i < answer.length()){
                        def+= (words+" ") ;

                    }else{
                        def+=words ;
                    }
                    if((did.get(words) == null)||(!did.get(words))){
                        this.occurrence_counter(words) ;
                        did.put(words,true) ;
                    }
                }
                i++ ;
            }

            while((i< answer.length())&&(answer.charAt(i) != '\n')){ // Filtering
                i++ ;
            }
            i++ ;
           // String mot = tagger.tagText(def);

          String mot = tagger2.tagString(def);
           // System.out.println(motCourant+" : "+def);
            this.mylist.add(motCourant+" : "+mot); //this adds an element to the list.
            motCourant ="" ;
            def ="";
        }
        System.out.println("Number of definitions analyzed : "+compteur);
    }

    public void saveAll () throws IOException {
        File outputFile = new File("taggedDefinitionsFile");
        FileWriter out = new FileWriter(outputFile);

        System.out.println("Saving tagged definitions ...");
        for (String s :  this.mylist) {
            out.write(s);
            out.write("\n");
        }
        out.close();
        System.out.println("Saved in "+outputFile.getAbsolutePath()) ;
        outputFile = new File("wordsOccFile");
        out = new FileWriter(outputFile);
        out.write(""+this.compteur);
        out.write("\n");
        Enumeration names;
        names = hashtable.keys();
        String key ;
        System.out.println("Saving words present in definitons ");
        while(names.hasMoreElements()) {
            key = (String) names.nextElement();
            out.write(key+" "+hashtable.get(key));
            out.write("\n");
        }

        out.close();
        System.out.println("Saved in "+outputFile.getAbsolutePath()) ;
    }
}
