package com.senseEmbedding  ;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * This is a standalone test that uses BioPortal SPARQL Endpoint without any extra libraries.
 * The result set format is CSV.
 */
public class QuerySparqlJava {

    private String service = null;
    private String apikey = null;
    String query = null ;
    public QuerySparqlJava (String service, String apikey,String request) {
        this.service = service;
        this.apikey = apikey;
        this.query = request ;
    }

    /**
     * fill in the writer with the query result and retirns the number of results.
     * @param acceptFormat
     * @param outputWriter
     * @return
     * @throws Exception
     */
    private int executeEffectiveQuery( String effectiveQuery, String acceptFormat, Writer outputWriter) throws Exception {
        String httpQueryString = String.format("query=%s&apikey=%s",
                URLEncoder.encode(effectiveQuery, "UTF-8"),
                URLEncoder.encode(this.apikey, "UTF-8"));

        URL url = new URL(this.service + "?" + httpQueryString);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", acceptFormat);

        conn.connect();
        InputStream in = conn.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        int nblines = 0;
        // Read the first line which contains the result's variable names.
        reader.readLine();
        String line = null;
        while ((line = reader.readLine())!=null) {
            outputWriter.append(line);
            outputWriter.append("\n");
            nblines++;
        }
        conn.disconnect();
        return nblines;
    }

    public int executeQuery(String acceptFormat, Writer outputWriter) throws Exception {
        return executeEffectiveQuery (this.query, acceptFormat, outputWriter);
    }

    public int executeLongQuery(String acceptFormat, Writer outputWriter) throws Exception {
        int chunks = 10000;
        int offset = 0;
        int nbResult = 0;
        int nbTotalResult = 0;
        String currentQuery = this.query + " limit " + chunks + " offset " + offset;
        while ((nbResult = executeEffectiveQuery(currentQuery, acceptFormat, outputWriter)) >= chunks) {
            nbTotalResult += nbResult;
            offset += chunks;
            currentQuery = this.query + " limit " + chunks + " offset " + offset;
        }
        nbTotalResult += nbResult;
        return nbTotalResult;
    }

}