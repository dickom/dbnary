package com.senseEmbedding;

/**
 * @author Mamadou DICKO
 */

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

public class SenseGenerator {

    private Hashtable<String, Double> idfValues ;
    private Hashtable <String, double []> outComes ;
   // private Hashtable<String, double> presentWords ;
    private Hashtable<String,  double [] > pretrainedVectors ;
    private int base ;
    private String path ;

    public SenseGenerator(String vectorsSavingPath, int vectorsDimension){

        this.idfValues = new Hashtable<String, Double>();
        this.idfValues = new Hashtable<String, Double> ();
        this.outComes = new Hashtable <String, double []> () ;
        this.pretrainedVectors = new Hashtable<String,double [] > ();
        this.base = vectorsDimension ;
        this.path = vectorsSavingPath ;
    }

    public void recoverPretrainedWordsVectors(String pretrainedWordsVectorsPath) throws IOException {
        System.out.println("RECOVERING PRE-TRAINED WORDS VECTORS ...");
        File inputFile = new File(pretrainedWordsVectorsPath) ;

        FileReader in = new FileReader(inputFile);
        int c;
        String mot = "" ;
        String mot2 ="" ;
        int compteur = 0 ;
        while ((c = in.read()) != '\n') {}

        while ((c = in.read()) != -1) {

            while (c != ' ') {
                mot += (char)c ;
                c = in.read() ;
            }
            while (c == ' ') {
                c = in.read() ;
            }
            int count = 0 ;

            double [] coordinatesArray = new double [ this.base] ;

            while(count != this.base){

                while ((c != ' ')&&(c!='\n')) {
                    mot2 += (char)c ;
                    c = in.read() ;
                }
                coordinatesArray[count] = Double.parseDouble(mot2) ;
                while ((c == ' ')) {
                    c = in.read() ;
                }
                count++ ;
                mot2 = "" ;

            }
            while (c != '\n') {
                c = in.read() ;
            }
//            System.out.println("Mot "+mot);
            this.pretrainedVectors.put(mot,coordinatesArray) ;
            compteur++ ;
            if((compteur%10000)==0){
                System.out.println("Analyzed : " + compteur) ;
            }
            mot = "" ;
            mot2 = "" ;
        }
        System.out.println("Analyzed : " + compteur) ;
        System.out.println("END OF THE RECOVERY");
    }

    public boolean estChiffre(char c){
        return ((c<='9')&(c>='0')) ;
    }

    public String realPart(String word){

        int i = 0 ;

        i = 0 ;
        while((i != word.length ())&&(!estChiffre (word.charAt(i)))){ // // recovering current word senseNumber
            i++ ;
        }
        while((i < word.length ())&&estChiffre (word.charAt(i))){ //
            i++ ;
        }

        while((i < word.length ())&&(word.charAt(i) != '_')){
            i++;
        }

        while((i < word.length ())&&(word.charAt(i) == '_')){
            i++ ;
        }

        int starting = i ;

        i = word.length ()-1 ;

        while((i > 0)&&(word.charAt(i) != '_')){
            i-- ;
        }

        while((i >0)&&(word.charAt(i) == '_')){
            i-- ;
        }
        while((i !=0)&&(word.charAt(i) != '_')){
            i-- ;
        }

        while((i !=0)&&(word.charAt(i) == '_')){
            i-- ;
        }
        String currentWord = "" ;
        while(starting <= i){ // recovering current word  name without the sense number
            currentWord += word.charAt(starting);
            starting++ ;
        }
        System.out.println ("current word is "+currentWord);
        return currentWord ;
    }

    public void recoverWordsData() throws IOException {
        System.out.println("SENSES VECTORS CALCULATING ...");
        File inputFile = new File("taggedDefinitionsFile");
        FileReader in = new FileReader(inputFile);
        int c;
      //  int compteur = 0 , compteur2 = 0 ;
        double weightPos = 0 ;
        while ((c = in.read()) != -1) {
            String mot = "";
            while ((c ) != ':') {
                mot += (char)c ;
                c = in.read() ;
            }
            while ((c = in.read()) == ' ') {}

            while (c != '\n') {
                String motC = "" ;
                String pos = "" ;
                while(c != '_'){
                    motC += (char)c ;
                    c = in.read() ;
                }
                c = in.read() ;
                while(c != ' '){
                    pos+= (char)c ;
                    c = in.read() ;
                }
                while(c == ' '){
                    c = in.read() ;
                }
                weightPos = this.posValue(pos) ;
                int indicateur =1 ;
                boolean avis_fin = (c=='\n') ;
                if(this.idfValues.get(motC) != null ){
                    indicateur = 0 ;
                    double calcul = (this.idfValues.get(motC)*weightPos) ;

                        if(this.pretrainedVectors.get(motC) != null){
                            if(this.outComes.get(mot) != null ){
                                this.outComes.put(mot,this.somme(mot,this.outComes.get(mot),this.pretrainedVectors.get(motC),calcul,avis_fin)) ;
                            }
                            else
                            {
                                this.outComes.put(mot,this.somme(mot,new double [this.base],this.pretrainedVectors.get(motC),calcul,avis_fin)) ;
                            }

                        }
                        else
                        {
                            if(this.pretrainedVectors.get(motC.toLowerCase ()) != null){
                                if(this.outComes.get(mot) != null ){
                                    this.outComes.put(mot,this.somme(mot,this.outComes.get(mot),this.pretrainedVectors.get(motC.toLowerCase ()),calcul,avis_fin)) ;
                                }
                                else
                                {
                                    this.outComes.put(mot,this.somme(mot,new double [this.base],this.pretrainedVectors.get(motC.toLowerCase ()),calcul,avis_fin)) ;
                                }
                            }
                            else
                            {
                                if(this.pretrainedVectors.get(this.realPart(mot)) != null){
                                    this.outComes.put(mot,this.pretrainedVectors.get(this.realPart(mot))) ;
                                }
                                else if (this.pretrainedVectors.get(this.realPart(mot).toLowerCase ()) != null){
                                    this.outComes.put(mot,this.pretrainedVectors.get(this.realPart(mot).toLowerCase ())) ;
                                }
                                else
                                {
                                    this.outComes.remove (mot) ;
                                }
                            }

                        }

                }
            }
        }
        System.out.println("Senses calculated");

    }
    public double [] somme (String mot , double [] tab1 , double [] tab2 , double coef ,boolean avis_fin) {

        double resultat [] = new double [this.base] ;

        if(avis_fin) {
            double norme = 0;
            if(tab2 == null){
                resultat = tab1 ;
                int i =  0 ;
                while(i != tab1.length){
                    norme +=(resultat[i]*resultat[i]) ;
                    i++ ;
                }

            }
            else
            {
                if(tab1 == null){
                    int i =  0 ;
                    while(i != tab2.length){
                        resultat[i] = coef*tab2[i] ;
                        norme +=(resultat[i]*resultat[i]) ;
                        i++ ;
                    }
                }
                else
                {
                    if(tab1.length == tab2.length){
                        int i =  0 ;
                        while(i != tab1.length){
                            tab1 [i] += (coef*tab2[i]) ;
                            norme +=(tab1[i]*tab1[i]) ;
                            i++ ;
                        }
                        resultat = tab1 ;
                    }
                }
            }
            norme = Math.sqrt(norme) ;
            if(norme != 0){
                int i =  0 ;
                while(i != resultat.length){
                    resultat [i] = resultat[i]/norme ;
                    i++ ;
                }
            }
            else
            {
                if(this.pretrainedVectors.get(this.realPart(mot)) != null){
                    this.outComes.put(mot,this.pretrainedVectors.get(this.realPart(mot))) ;
                }
                else if (this.pretrainedVectors.get(this.realPart(mot).toLowerCase ()) != null){
                    this.outComes.put(mot,this.pretrainedVectors.get(this.realPart(mot).toLowerCase ())) ;
                }
                else
                {
                    this.outComes.remove (mot) ;
                }

            }

        }
        else
        {
            if(tab2 == null){
                resultat = tab1 ;
            }
            else
            {
                if(tab1 == null) {
                    int i = 0;
                    while (i != tab2.length) {
                        resultat[i] = coef * tab2[i];
                        i++;
                    }
                }
                else
                {
                    if(tab1.length == tab2.length){
                        int i =  0 ;
                        while(i != tab1.length){
                            tab1 [i] += (coef*tab2[i]) ;
                            i++ ;
                        }
                        resultat = tab1 ;
                    }
                }
            }
        }
        return resultat ;
    }
    public double posValue(String pos){
        double weigth = 0 ;

        switch (pos) {
            case "CC":
                weigth = 0.7;
                break;
            case "CD":
                weigth = 0.8;
                break;
            case "DT":
                weigth = 0.7;
                break;
            case "EX":
                weigth = 0.7;
                break;
            case "FW":
                weigth = 0.7;
                break;
            case "IN":
                weigth =0.7 ;
                break;
            case "JJ":
                weigth = 0.7;
                break;
            case "JJR":
                weigth = 0.7 ;
                break;
            case "JJS":
                weigth = 0.8 ;
                break;
            case "LS":
                weigth = 0.7;
                break;
            case "MD":
                weigth = 1.2;
                break;
            case "NN":
                weigth = 0.8;
                break;
            case "NNS":
                weigth = 1.0;
                break;
            case "NNP":
                weigth = 0.8;
                break;
            case "NNPS":
                weigth = 0.8;
                break;
            case "PDT":
                weigth = 0.7 ;
                break;
            case "POS":
                weigth = 0.7;
                break;
            case "PRP":
                weigth = 0.7;
                break;
            case "PRP$":
                weigth = 0.7 ;
                break;
            case "RB":
                weigth = 1.3;
                break;
            case "RBR":
                weigth =1.2 ;
                break;
            case "RBS":
                weigth = 1.0;
                break;
            case "RP":
                weigth = 1.2;
                break;
            case "SYM":
                weigth = 0.7;
                break;
            case "TO":
                weigth = 0.8;
                break;
            case "UH":
                weigth = 0.7;
                break;
            case "VB":
                weigth = 1.2;
                break;
            case "VBD":
                weigth = 1.2;
                break;
            case "VBG":
                weigth = 1.1 ;
                break;
            case "VBN":
                weigth = 0.8 ;
                break;
            case "VBP":
                weigth =1.2 ;
                break;
            case "VBZ":
                weigth = 1.2;
                break;
            case "WDT":
                weigth =  0.7;
                break;
            case "WP":
                weigth = 0.7 ;
                break;
            case "WP$":
                weigth =0.7 ;
                break;
            case "WRB":
                weigth = 1.3;
                break;
            default:
                weigth =0 ;
        }
        return weigth ;
    }
    public void recoverWordsOcc() throws IOException {
        System.out.println("RECOVERING WORDS  WEIGHTS ...");
        File inputFile = new File("wordsOccFile");
      //  System.out.println ("fiel path "+inputFile.getAbsolutePath ());
        FileReader in = new FileReader(inputFile);
        int c;
        String mot = "" ;
        String mot2 ="" ;
        int occ = 0 ;
        String count = "" ;
        int totalNumber = 0 ;
        while ((c = in.read()) != '\n') {
           count+= (char)c ;
        }
        totalNumber = Integer.parseInt(count) ;

        while ((c = in.read()) != -1) {
            if(c != '\n'){
                mot += (char)c ;
            }

            while ((c = in.read()) != ' ') {
                mot += (char)c ;
            }
            while (c == ' ') {
                c = in.read() ;
            }

            while (c != '\n') {
                mot2 += (char)c ;
                c = in.read() ;
            }

            this.idfValues.put(mot,Math.log(totalNumber/Double.parseDouble(mot2)));
            mot = "" ;
            mot2 = "" ;
        }

        System.out.println("END OF RECOVERY ...");
    }

    public void saveVectors () throws IOException {
        File outputFile = new File(this.path);
        FileWriter out = new FileWriter(outputFile);

        Enumeration names;
        names = outComes.keys();
        String key ;
        System.out.println("Saving the file containing senses vectors ...");
        while(names.hasMoreElements()) {
            key = (String) names.nextElement();
            double values [] = outComes.get(key) ;
            int i = 0 ;
            out.write(key+":");
            while(i != values.length){
                out.write(values[i]+" ");
                i++ ;
            }
            out.write("\n");
        }
        out.close();
        System.out.println("Saved in "+outputFile.getAbsolutePath()) ;
        System.out.println("END.");
    }

}
