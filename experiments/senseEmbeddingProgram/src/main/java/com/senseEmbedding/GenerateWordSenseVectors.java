package com.senseEmbedding  ;

/**
 * @author Mamadou DICKO
 * This is an example
 * It can help you to understand how this program works.
 */
/*
import edu.stanford.nlp.ling.HasWord;
import edu.stanford.nlp.ling.SentenceUtils;
import edu.stanford.nlp.ling.TaggedWord;
import edu.stanford.nlp.tagger.maxent.MaxentTagger; */

public class GenerateWordSenseVectors {


    public static void main(String[] args) throws Exception {

        SenseGenerator sense = new SenseGenerator ("GreekSenseVectors",300);
        sense.recoverWordsOcc ();
        sense.recoverPretrainedWordsVectors("/Users/dickoma/Downloads/cc.el.300.vec") ;
        sense.recoverWordsData() ;
        sense.saveVectors() ;

    }
}
