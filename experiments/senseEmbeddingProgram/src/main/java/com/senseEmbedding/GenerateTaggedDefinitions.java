package com.senseEmbedding;

/**
 * @author Mamadou DICKO
 * This is an example
 * It can help you to understand how this program works.
 */


import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb.TDBFactory;

public class GenerateTaggedDefinitions {

  private static Map<String,String> taggers = new HashMap<>();
  static {
    taggers.put("fra", "french.tagger");
    taggers.put("eng", "wsj-0-18-left3words-nodistsim.tagger");
    taggers.put("ell", "wsj-0-18-left3words-nodistsim.tagger");
    taggers.put("deu", "german-fast.tagger");

  }

  public static void main(String[] args) throws Exception {
    String lg = args[0];
    String tdbPath = args[1];

    Dataset dataset = TDBFactory.createDataset(tdbPath);
    dataset.begin(ReadWrite.READ);

    Model model = dataset.getNamedModel("http://kaiko.getalp.org/dbnary/" + lg);

    MaxentTagger tagger2 = new MaxentTagger("models/" + taggers.get(lg));

    Map<String, String> ssid2taggedDef = new HashMap<>();

    String sparqlService = "http://kaiko.getalp.org/sparql";
    String apikey = "dbnary";
    String queryString = "PREFIX dbnary: <http://kaiko.getalp.org/dbnary#>\n"
        + "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n"
        + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
        + "PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>\n"
        + "PREFIX dct: <http://purl.org/dc/terms/>\n"
        + "PREFIX dbnary: <http://kaiko.getalp.org/dbnary#>\n"
        + "PREFIX dbnary-bul: <http://kaiko.getalp.org/dbnary/bul/>\n"
        + "PREFIX dbnary-deu: <http://kaiko.getalp.org/dbnary/deu/>\n"
        + "PREFIX dbnary-ell: <http://kaiko.getalp.org/dbnary/ell/>\n"
        + "PREFIX dbnary-eng: <http://kaiko.getalp.org/dbnary/eng/>\n"
        + "PREFIX dbnary-fin: <http://kaiko.getalp.org/dbnary/fin/>\n"
        + "PREFIX dbnary-fra: <http://kaiko.getalp.org/dbnary/fra/>\n"
        + "PREFIX dbnary-ind: <http://kaiko.getalp.org/dbnary/ind/>\n"
        + "PREFIX dbnary-ita: <http://kaiko.getalp.org/dbnary/ita/>\n"
        + "PREFIX dbnary-jpn: <http://kaiko.getalp.org/dbnary/jpn/>\n"
        + "PREFIX dbnary-lat: <http://kaiko.getalp.org/dbnary/lat/>\n"
        + "PREFIX dbnary-lit: <http://kaiko.getalp.org/dbnary/lit/>\n"
        + "PREFIX dbnary-mlg: <http://kaiko.getalp.org/dbnary/mlg/>\n"
        + "PREFIX dbnary-nld: <http://kaiko.getalp.org/dbnary/nld/>\n"
        + "PREFIX dbnary-nor: <http://kaiko.getalp.org/dbnary/nor/>\n"
        + "PREFIX dbnary-pol: <http://kaiko.getalp.org/dbnary/pol/>\n"
        + "PREFIX dbnary-por: <http://kaiko.getalp.org/dbnary/por/>\n"
        + "PREFIX dbnary-rus: <http://kaiko.getalp.org/dbnary/rus/>\n"
        + "PREFIX dbnary-shr: <http://kaiko.getalp.org/dbnary/shr/>\n"
        + "PREFIX dbnary-spa: <http://kaiko.getalp.org/dbnary/spa/>\n"
        + "PREFIX dbnary-swe: <http://kaiko.getalp.org/dbnary/swe/>\n"
        + "PREFIX dbnary-tur: <http://kaiko.getalp.org/dbnary/tur/>\n"
        + "PREFIX lexvo: <http://lexvo.org/id/iso639-3/>\n"

        + "SELECT ?ssid ?def WHERE { \n"
        + "?le ontolex:sense ?sid ; \n"
        + "dct:language lexvo:" + lg + ". \n"
        + "?sid skos:definition / rdf:value ?def . \n"
        + " bind(strafter(str(?sid),str(dbnary-" + lg + ":)) as ?ssid) \n"
        + "} ";

    Query query = QueryFactory.create(queryString) ;
    StringWriter w = new StringWriter();

    try (QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
      ResultSet results = qexec.execSelect() ;
      int nbres = 0;
      for ( ; results.hasNext() ; )
      {
        nbres++;
        QuerySolution soln = results.nextSolution() ;
        Literal ssid = soln.getLiteral("ssid") ;   // Get a result variable - must be a literal
        Literal def = soln.getLiteral("def") ;   // Get a result variable - must be a literal

        ssid2taggedDef.put(ssid.getString(), tagger2.tagString(def.getString()));
        if (nbres%1000 == 0) {
          System.err.println(ssid.getString() + ": " + ssid2taggedDef.get(ssid.getString()));
        }
      }
    }

    // Do something with the results

    dataset.end();

  }
}
