package org.getalp.dbnary.enhancer.similarity;

/**
 * @author Mamadou DICKO
 */
public class CosinusSimilarity {

    public double cosinusSimilarity(Double firstVector [] , Double secondVector []){

        if((firstVector.length != 0) && (firstVector.length != 0 )){
            double fxy = 0 ;
            double qx = 0 ;
            double qy = 0 ;

            int i = 0 ;
            if(firstVector.length == secondVector.length){
                while(i != firstVector.length){
                    fxy += (firstVector[i].doubleValue ()*secondVector[i].doubleValue ()) ;
                    qx += (firstVector[i].doubleValue ()*firstVector[i].doubleValue ()) ;
                    qy += (secondVector[i].doubleValue ()*secondVector[i].doubleValue ()) ;
                    i++ ;
                }
            }
            if((qx != 0 )&&(qy !=0)){
                return  Math.sqrt(fxy/Math.sqrt(qx*qy)) ;
            }
            else
            {
                return 0 ;
            }

        }
        else
        {
            return 0 ;
        }

    }

}
