package org.getalp.dbnary.enhancer.disambiguation;

import com.wcohen.ss.ScaledLevenstein;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.RDF;
import org.getalp.dbnary.DBnaryOnt;
import org.getalp.dbnary.OntolexOnt;
import org.getalp.dbnary.SkosOnt;
import org.getalp.dbnary.enhancer.similarity.CosinusSimilarity;
import org.getalp.dbnary.enhancer.similarity.string.TverskiIndex;

import static org.getalp.dbnary.LexinfoOnt.gloss;

public class SenseVectorsBasedTranslationDisambiguationMethod implements DisambiguationMethod {

    private double delta;
    // private double alpha;
    // private double beta;

    public SenseVectorsBasedTranslationDisambiguationMethod() {}

    private class WeigthedSense {

        protected double weight;
        protected Resource sense;

        public WeigthedSense(double weight, Resource sense) {
            super();
            this.weight = weight;
            this.sense = sense;
        }
    }


    public Connection getConnection() throws SQLException {
        Connection conn = null ;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/wordSenseVectors";
            String user = "root";
            String password = "";
            conn = DriverManager.getConnection(url, user, password);
        }
        catch(Exception e) {
            System.out.println (e);
        }
        return conn;
    }

    public Double [] getCoordinates (String word, String tableName) throws SQLException {
        Connection con ;
        ResultSet résultats ;
        String request = "";
        java.sql.Statement statement ;
        ResultSet rs ;
        con = this.getConnection () ;

        PreparedStatement preparedStmt ;

        String query = " SELECT Vectors from "+tableName
                + " WHERE  Words = ? ";

        preparedStmt = con.prepareStatement(query);
        String vector = "" ;

        try{
            preparedStmt.setString (1, word);

            rs = preparedStmt.executeQuery ();

            if (rs.next()) {
                vector = rs.getString("Vectors") ;
            }

        }
        catch (ArrayIndexOutOfBoundsException e) {
            System.err.println ("OutOfBoundsError "+e.getMessage ());
        }

        return (Arrays.stream(vector.split (" "))
                .map(Double::valueOf)
                .toArray(Double[]::new));
    }

    @Override
    public Set<Resource> selectWordSenses(Resource lexicalEntry, Object context)
            throws InvalidContextException, InvalidEntryException, SQLException {
        HashSet<Resource> res = new HashSet<Resource>();

        if (!lexicalEntry.hasProperty(RDF.type, OntolexOnt.LexicalEntry)
                && !lexicalEntry.hasProperty(RDF.type, OntolexOnt.Word)
                && !lexicalEntry.hasProperty(RDF.type, OntolexOnt.MultiWordExpression)) {
            throw new InvalidEntryException("Expecting a LEMON Lexical Entry.");
        }
        if (context instanceof Resource) {
            Resource trans = (Resource) context;
            if (!trans.hasProperty(RDF.type, DBnaryOnt.Translation)) {
                throw new InvalidContextException("Expecting a DBnary Translation Resource.");
            }

            ArrayList<WeigthedSense> weightedList = new ArrayList<WeigthedSense>();

            Double [] coordinates = this.getCoordinates(lexicalEntry.toString(),"") ;
            // get a list of wordsenses, sorted by decreasing similarity.
            StmtIterator senses = lexicalEntry.listProperties(OntolexOnt.sense);

            CosinusSimilarity similarity = new CosinusSimilarity () ;

            while (senses.hasNext()) {
                System.out.println(senses.next().toString()) ;
                Statement nextSense = senses.next();
                Resource wordsense = nextSense.getResource();
                Statement dRef = wordsense.getProperty(SkosOnt.definition);
                Statement dVal = dRef.getProperty(RDF.value);
                String deftext = dVal.getObject().toString();

                double sim = similarity.cosinusSimilarity (coordinates, new Double[0]);

                insert(weightedList, wordsense, sim);
            }

            if (weightedList.size() == 0) {
                return res;
            }

            int i = 0;
        //    double worstScore = weightedList.get(0).weight - delta;
            while (i != weightedList.size()) {
                res.add(weightedList.get(i).sense);
                i++;
            }

        } else {
            throw new InvalidContextException("Expecting a JENA Resource.");
        }

        return res;
    }

    private void insert(ArrayList<WeigthedSense> weightedList, Resource wordsense, double sim) {
        weightedList.add(null);
        int i = weightedList.size() - 1;
        while (i != 0 && weightedList.get(i - 1).weight < sim) {
            weightedList.set(i, weightedList.get(i - 1));
            i--;
        }
        weightedList.set(i, new WeigthedSense(sim, wordsense));
    }

}
